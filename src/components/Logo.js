
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  Image
  
 
} from 'react-native';
export default class Logo extends Component {
    render() {
        return( 
            <View style={styles.container}>
                <Image style={{width:200, height: 150}}
                 source={require('../images/logo.png')}/>
                <Text style={styles.logotext}>Welcome To Easy Ledgers </Text>
            </View>
            
        )
    }
}

const styles=StyleSheet.create({
    container: {  
      
      flexGrow:1,
      justifyContent: 'flex-end',
      alignItems: 'center'
      }, 
      logotext: {
        marginVertical: 15,
          fontSize: 25,
          color: '#c67100'

      }
    });