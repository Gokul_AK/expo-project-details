import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  TouchableOpacity
  
  
} from 'react-native';

import Logo from '../components/Logo';
import Form from '../components/Form';


export default class Login extends Component {
 
	render() {
		return(
			<View style={styles.container}>
                <Logo/>
                <Form type="Login"/>
                <View style={styles.signupTextCont}>
                  <Text style={styles.signupText}>Don't have an account?</Text>
                  <TouchableOpacity onPress={this.signup}>
                    <Text style={styles.signupButton}> Signup</Text>
                    </TouchableOpacity>
                </View>
            </View>
            )
    }
}

const styles=StyleSheet.create({
  container: {
    backgroundColor:'#455a64',
    flex:1,
    alignItems:'center',
    justifyContent:'center'
    },
    signupTextCont:{
      flex:1,
    alignItems:'flex-end',
    justifyContent:'center',
    paddingVertical:16,
    flexDirection:'row'

    },
    signupText:{
      color:'rgba(255,255,255,0.6)',
      fontSize:20
    },
    signupButton:{
      color:'#ffffff',
      fontSize:20,
      fontWeight:'900' 
    }
  });